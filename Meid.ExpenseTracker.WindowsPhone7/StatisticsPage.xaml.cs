﻿#region Imports
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Meid.ExpenseTracker.WindowsPhone7.ViewModels;

using Microsoft.Phone.Controls;
#endregion

namespace Meid.ExpenseTracker.WindowsPhone7
{
    public partial class StatisticsPage : PhoneApplicationPage
    {
        #region Fields, Constructor
        public static readonly Uri NavigationUri = new Uri("/StatisticsPage.xaml", UriKind.Relative);

        public StatisticsPage()
        {
            InitializeComponent();

            if(App.ViewModel.Statistics == null
                || !App.ViewModel.Statistics.IsInitialized)
            {
                WaitCallback initStatsWaitCallback = new WaitCallback(o =>
                {
                    App.ViewModel.InitializeStatistics();
                    Dispatcher.BeginInvoke(() => this.DataContext = App.ViewModel.Statistics);
                });
                ThreadPool.QueueUserWorkItem(initStatsWaitCallback);
            }
            else
            {
                this.DataContext = App.ViewModel.Statistics;
            }
        }
        #endregion

        #region Control Event Handlers
        private void overviewAppButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(MainPage.NavigationUri);
        }
        #endregion
    }
}