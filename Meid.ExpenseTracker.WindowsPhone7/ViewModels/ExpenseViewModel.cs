﻿#region Imports
using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using WebberCross.Obelisk;
#endregion

namespace Meid.ExpenseTracker.WindowsPhone7.ViewModels
{
    public class ExpenseViewModel : INotifyPropertyChanged
    {
        #region Fields, Constructor
#pragma warning disable 67
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67

        public ExpenseViewModel()
        {
            this.ID = Guid.NewGuid();
            this.Amount = 0;
            this.Description = "";
            this.Date = DateTime.Now;
            this.ItemState = ItemState.Unknown;
        }
        #endregion

        #region Public Properties
        [Persist(PersistMode.Both)]
        public Guid ID { get; set; }

        [Persist(PersistMode.Both)]
        public decimal Amount { get; set; }

        [Persist(PersistMode.Both)]
        public string Description { get; set; }

        [Persist(PersistMode.Both)]
        public DateTime Date { get; set; }

        [Persist(PersistMode.Both)]
        public ItemState ItemState { get; set; }
        #endregion
    }
}
