﻿#region Imports
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
#endregion

namespace Meid.ExpenseTracker.WindowsPhone7.ViewModels
{
    public sealed class StatisticsViewModel : INotifyPropertyChanged
    {
        #region Fields, Constructor
#pragma warning disable 67
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67

        private static readonly IEnumerable<ExpenseViewModel> emptyExpenseCollection = new ExpenseViewModel[0];

        private readonly object initializationLock;

        private MainViewModel mainViewModel;
        private IDictionary<DateTime, decimal> totalsByDay;

        public StatisticsViewModel()
        {
            this.IsInitialized = false;
            this.initializationLock = new object();
        }
        #endregion

        #region Public Methods
        public void Initialize(MainViewModel mainViewModel)
        {
            if(!this.IsInitialized)
            {
                lock(this.initializationLock)
                {
                    if(!this.IsInitialized)
                    {
                        var qry = from e in mainViewModel.Expenses
                                  group e by e.Date.Date into g
                                  select new
                                  {
                                      Day = g.Key,
                                      Total = g.Sum(x => x.Amount)
                                  };
                        this.totalsByDay = qry.ToDictionary(q => q.Day,
                            q => q.Total);

                        UpdateDateTotals();

                        this.mainViewModel = mainViewModel;
                        this.mainViewModel.Expenses.CollectionChanged += Expenses_CollectionChanged;
                        this.IsInitialized = true;
                    }
                }
            }
        }
        #endregion

        #region Event Handlers
        private void Expenses_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var oldExpenses = e.OldItems == null ? emptyExpenseCollection : e.OldItems.Cast<ExpenseViewModel>();
            var newExpenses = e.NewItems == null ? emptyExpenseCollection : e.NewItems.Cast<ExpenseViewModel>();

            switch(e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    AddToTotals(newExpenses);
                    break;

                case NotifyCollectionChangedAction.Remove:
                    RemoveFromTotals(oldExpenses);
                    break;

                case NotifyCollectionChangedAction.Replace:
                    AddToTotals(newExpenses);
                    RemoveFromTotals(oldExpenses);
                    break;

                case NotifyCollectionChangedAction.Reset:
                    lock(this.initializationLock)
                    {
                        this.IsInitialized = false;
                        Initialize(this.mainViewModel);
                    }
                    break;
            }

            UpdateDateTotals();
        }
        #endregion

        #region Private Helper Methods
        private void AddToTotals(IEnumerable<ExpenseViewModel> expenses)
        {
            foreach(var exp in expenses)
            {
                var date = exp.Date.Date;
                if(this.totalsByDay.ContainsKey(date))
                    this.totalsByDay[date] += exp.Amount;
                else
                    this.totalsByDay[date] = exp.Amount;
            }
        }

        private void RemoveFromTotals(IEnumerable<ExpenseViewModel> expenses)
        {
            foreach(var exp in expenses)
            {
                var date = exp.Date.Date;
                this.totalsByDay[date] -= exp.Amount;
            }
        }

        private void UpdateDateTotals()
        {
            var qry = from p in this.totalsByDay
                      orderby p.Key descending
                      select new DateTotal(p.Key, p.Value);
            this.DateTotals = qry.ToList();
        }
        #endregion

        #region Public Properties
        public IList<DateTotal> DateTotals { get; private set; }

        public bool IsInitialized { get; private set; }
        #endregion

        #region DateTotal Inner Class
        public sealed class DateTotal
        {
            public DateTotal(DateTime date, decimal total)
            {
                this.Date = date;
                this.Total = total;
            }

            public DateTime Date { get; private set; }

            public decimal Total { get; private set; }
        }
        #endregion
    }
}
