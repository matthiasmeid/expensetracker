﻿#region Imports
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using WebberCross.Obelisk;
#endregion

namespace Meid.ExpenseTracker.WindowsPhone7.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Events, Fields, Constructor
#pragma warning disable 67
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 67

        private static readonly IEnumerable<ExpenseViewModel> emptyExpenseCollection = new ExpenseViewModel[0];

        private ExpenseViewModel newExpense;
        private ObservableCollection<ExpenseViewModel> expenses;

        public MainViewModel()
        {
            // Use properties instead of backing fields. View wouldn't get
            // notified otherwise
            this.Expenses = new ObservableCollection<ExpenseViewModel>();
            this.NewExpense = new ExpenseViewModel() { ItemState = ItemState.New };

            this.PropertyChanged += new PropertyChangedEventHandler(MainViewModel_PropertyChanged);

#if DEBUG
            this.Expenses.Add(new ExpenseViewModel()
            {
                Amount = (decimal)5.5,
                Description = "The News",
                Date = DateTime.Now.AddDays(-1)
            });
            this.Expenses.Add(new ExpenseViewModel()
            {
                Amount = (decimal)2.8,
                Description = "Coffee",
                Date = DateTime.Now.AddMinutes(-130)
            });
            this.Expenses.Add(new ExpenseViewModel()
            {
                Amount = (decimal)5.5,
                Description = "Train Ticket",
                Date = DateTime.Now.AddMinutes(-90)
            });
#endif
        }
        #endregion

        #region Property Changed and Collection Changed Handlers
        void Expenses_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var oldExpenses = e.OldItems == null ? emptyExpenseCollection : e.OldItems.Cast<ExpenseViewModel>();
            var newExpenses = e.NewItems == null ? emptyExpenseCollection : e.NewItems.Cast<ExpenseViewModel>();

            var oldExpensesToday = oldExpenses.Where(x => x.Date.Date == DateTime.Today);
            var newExpensesToday = newExpenses.Where(x => x.Date.Date == DateTime.Today);

            switch(e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    this.TotalAmount += newExpenses.Sum(i => i.Amount);
                    this.TotalAmountToday += newExpensesToday.Sum(i => i.Amount);
                    break;

                case NotifyCollectionChangedAction.Remove:
                    this.TotalAmount -= oldExpenses.Sum(i => i.Amount);
                    this.TotalAmountToday -= oldExpensesToday.Sum(i => i.Amount);
                    break;

                case NotifyCollectionChangedAction.Replace:
                    this.TotalAmount += oldExpenses.Sum(i => -i.Amount)
                        + newExpenses.Sum(i => i.Amount);
                    this.TotalAmountToday += oldExpensesToday.Sum(i => -i.Amount)
                        + newExpensesToday.Sum(i => i.Amount);
                    break;

                case NotifyCollectionChangedAction.Reset:
                    this.TotalAmount = this.Expenses.Sum(i => i.Amount);
                    this.TotalAmountToday = this.Expenses.Where(x => x.Date.Date == DateTime.Today).Sum(i => i.Amount);
                    break;
            }
        }

        void MainViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "NewExpense":
                    NewExpense_PropertyChanged(this, new PropertyChangedEventArgs(""));
                    break;

                case "Expenses":
                    if(this.Expenses == null) this.TotalAmount = 0;
                    else this.TotalAmount = this.Expenses.Sum(i => i.Amount);
                    break;
            }
        }

        void NewExpense_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.CanAddNewExpense = this.NewExpense != null
                    && this.NewExpense.Amount > 0
                    && !String.IsNullOrEmpty(this.NewExpense.Description);
        }
        #endregion

        #region Public Methods
        public void AddNewExpense()
        {
            var newExpense = this.NewExpense;
            newExpense.ItemState = ItemState.Unknown;

#if DEBUG
            newExpense.Date = newExpense.Date.AddDays((int)newExpense.Amount % 10);
#endif

            this.Expenses.Insert(0, newExpense);

            this.NewExpense = new ExpenseViewModel() { ItemState = ItemState.New };
        }

        public ExpenseViewModel GetExpenseByID(Guid id)
        {
            // TODO: Make this O(1)
            return this.Expenses.First(e => e.ID == id);
        }

        public void InitializeStatistics()
        {
            if(this.Statistics == null)
            {
                this.Statistics = new StatisticsViewModel();
                this.Statistics.Initialize(this);
            }
        }
        #endregion

        #region Public Properties
        [Persist(PersistMode.None)]
        public bool CanAddNewExpense { get; set; }

        [Persist(PersistMode.TombstonedOnly)]
        public ExpenseViewModel NewExpense
        {
            get
            {
                return this.newExpense;
            }
            set
            {
                if(this.newExpense != null)
                    this.newExpense.PropertyChanged -= NewExpense_PropertyChanged;
                this.newExpense = value;
                this.newExpense.PropertyChanged += NewExpense_PropertyChanged;
            }
        }

        public ObservableCollection<ExpenseViewModel> Expenses
        {
            get
            {
                return this.expenses;
            }
            set
            {
                if(this.expenses != null)
                    this.expenses.CollectionChanged -= Expenses_CollectionChanged;
                this.expenses = value;
                this.expenses.CollectionChanged += Expenses_CollectionChanged;
            }
        }

        /// <summary>
        /// Gets or sets an array of expenses. For persistence only.
        /// </summary>
        [Persist(PersistMode.Both)]
        public ExpenseViewModel[] ExpenseArray
        {
            get
            {
                return this.Expenses.ToArray();
            }
            set
            {
                this.Expenses = new ObservableCollection<ExpenseViewModel>(value);
            }
        }

        [Persist(PersistMode.TombstonedOnly)]
        public StatisticsViewModel Statistics { get; private set; }

        [Persist(PersistMode.Both)]
        public decimal TotalAmount { get; private set; }

        [Persist(PersistMode.Both)]
        public decimal TotalAmountToday { get; private set; }
        #endregion
    }
}
