﻿#region Imports
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Meid.ExpenseTracker.WindowsPhone7.ViewModels;

using Microsoft.Phone.Controls;
#endregion

namespace Meid.ExpenseTracker.WindowsPhone7
{
    public partial class MainPage : PhoneApplicationPage
    {
        #region Fields, Constructor
        public static readonly Uri NavigationUri = new Uri("/MainPage.xaml", UriKind.Relative);

        private readonly BindingExpression descriptionBindingExpression;

        public MainPage()
        {
            InitializeComponent();

            this.DataContext = App.ViewModel;
            this.descriptionBindingExpression = DescriptionTextBox.GetBindingExpression(TextBox.TextProperty);
        }
        #endregion

        #region Control Event Handlers
        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            App.ViewModel.AddNewExpense();
        }

        private void ExpensesSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(e.AddedItems.Count == 1)
            {
                var item = e.AddedItems[0] as LongListSelector.LongListSelectorItem;
                if(item != null)
                {
                    var expense = (ExpenseViewModel)item.Item;
                    var uri = new Uri("/ExpenseDetail.xaml?selectedItem=" + expense.ID,
                        UriKind.Relative);
                    NavigationService.Navigate(uri);
                }

                ExpensesSelector.SelectedItem = null;
            }
        }

        private void statsAppButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(StatisticsPage.NavigationUri);
        }

        private void NewAmountTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            NewAmountTextBox.SelectAll();
        }

        private void DescriptionTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.descriptionBindingExpression.UpdateSource();
        }
        #endregion
    }
}