﻿#region Imports
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Meid.ExpenseTracker.WindowsPhone7.ViewModels;

using Microsoft.Phone.Controls;
#endregion

namespace Meid.ExpenseTracker.WindowsPhone7
{
    public partial class ExpenseDetail : PhoneApplicationPage
    {
        #region Constructor
        public ExpenseDetail()
        {
            InitializeComponent();
        }
        #endregion

        #region XAML Page Event Handlers
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string selectedItem;
            if(NavigationContext.QueryString.TryGetValue("selectedItem", out selectedItem))
            {
                var id = Guid.Parse(selectedItem);
                var expense = App.ViewModel.GetExpenseByID(id);
                this.DataContext = expense;

                var ratioWaitCallback = new WaitCallback(o =>
                {
                    var totalToday = App.ViewModel.Expenses.Where(x => x.Date.Date == DateTime.Today).Sum(y => y.Amount);
                    var ratio = expense.Amount / totalToday;
                    var ratioString = ratio.ToString("P");

                    Dispatcher.BeginInvoke(() => { RatioTextBlock.Text = ratioString; });
                });

                ThreadPool.QueueUserWorkItem(ratioWaitCallback);
            }
            base.OnNavigatedTo(e);
        }
        #endregion

        #region Control Event Handlers
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var expense = (ExpenseViewModel)this.DataContext;
            App.ViewModel.Expenses.Remove(expense);

            NavigationService.Navigate(MainPage.NavigationUri);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(MainPage.NavigationUri);
        }
        #endregion
    }
}